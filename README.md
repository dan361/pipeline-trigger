# GitLab Pipeline Trigger

A simple go tool that triggers the gitlab API to trigger a pipeline via HTTP POST.

### Why?

For reasons I do not understand GitLab allows Pipeline Schedules only once an hour, regardless of what you choose as cron schedule.
Maybe this is a paid feature? I was looking for a way to trigger a GitLab pipelines more frequently.

### Why not use curl or something else?

I am learning Go, so I wanted to develop a solution to learn. I am far away from being a good Go developer though ;)

### Prerequistes

Obtain a personal, project or group access to token to get your projectID:

```bash
export TOKEN=YOURTOKEN
curl -s -H "PRIVATE-TOKEN: $TOKEN "https://gitlab.com/api/v4/projects/your_username_or_group%2Fyour_project | jq .id
```

Create a [pipeline trigger token](https://docs.gitlab.com/ee/ci/triggers/#create-a-pipeline-trigger-token)

### Usage

It is a simple tool that takes a project ID, a pipeline trigger token and a branch that you want to run a pipeline on it.

You can run this tool simply by:

`go run main.go --project-id=YOUR_PROJECT_ID --token=YOUR_PIPELINE_TRIGGER_TOKEN [--branch=main]`

or simply build it: `go build -o plt main.go` and run `./plt --project-id=YOUR_PROJECT_ID --token=YOUR_PIPELINE_TRIGGER_TOKEN [--branch=main]`

I also built a docker image, so it is possible to run via docker, podman or Kubernetes, for example:

`docker run --rm -t registry.gitlab.com/dan361/pipeline-trigger:latest --project-id=YOUR_PROJECT_ID --token=YOUR_PIPELINE_TRIGGER_TOKEN [--branch=main]`

The default `--branch` value is always main and can be omitted.

For Kubernetes I wrote a simple CronJob which can be found [here](kubernetes/cronjob.yaml)

This CronJob uses a secret that needs to be created, for example:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: yoursecret
  namespace: yournamesspace
data:
  branch: main
  project_id: YOUR_PROJECT_ID
  token: YOUR_PIPELINE_TRIGGER_TOKEN
type: Opaque
```