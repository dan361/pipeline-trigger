# BUILD
FROM  golang:1.21.3 as BUILD

ENV GO111MODULE=on

WORKDIR /app

COPY go.mod .

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build


# BINARIES

FROM alpine:latest

COPY --from=BUILD /app/plt /app/plt

ENTRYPOINT ["/app/plt"]
