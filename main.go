package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func triggerPipeline(projectID, token, ref string) error {
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/trigger/pipeline?token=%s&ref=%s", projectID, token, ref)

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("Failed to trigger pipeline. Status code: %d", resp.StatusCode)
	}

	fmt.Println("Pipeline triggered successfully.")
	return nil
}

func printHelp() {
	fmt.Println("Usage: plt [options]")
	fmt.Println("Options:")
	fmt.Println("   --project-id    GitLab projectID")
	fmt.Println("   --token         GitLab pipeline trigger token")
	fmt.Println("   --branch        GitLab branch (default: main)")
	fmt.Println("   -h, --help      Show this help message")
}

func main() {
	projectID := flag.String("project-id", "", "GitLab project ID")
	pipelineTriggerToken := flag.String("token", "", "GitLab pipeline trigger token")
	ref := flag.String("branch", "main", "GitLab branch (default: main)")
	help := flag.Bool("h", false, "Show help message")
	help2 := flag.Bool("help", false, "Show help message")

	flag.Usage = func() {
		printHelp()
	}

	flag.Parse()

	if *help || *help2 {
		printHelp()
		os.Exit(0)
	}

	if *projectID == "" || *pipelineTriggerToken == "" {
		fmt.Println("Error: Both --project-id and --token are requred.")
		fmt.Println("Use -h or --help for usage information.")
		os.Exit(1)
	}

	err := triggerPipeline(*projectID, *pipelineTriggerToken, *ref)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		os.Exit(1)
	}
}
